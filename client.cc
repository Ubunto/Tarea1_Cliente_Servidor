#include <iostream>
#include <zmqpp/zmqpp.hpp>
#include <fstream>

using namespace std;
using namespace zmqpp;


void mostrar_matriz(int **Matriz, int fila, int columna);

void mostrar_inversa(double **Matriz, int fila, int columna);


int main(int argc, char const *argv[]) {
  
  if(argc != 4){
    cerr << "Error calling the program" << endl;
    return 1;
  }
  
  // initialize the 0MQ context
  context ctx;

  // generate a push socket (canal de comunicacion)
  socket s (ctx,socket_type::request);
  /* tipo de socket
      push - enviar mensajes , pero no recibir mensajes
      pull - recibir mensajes, pero no enviar mensajes
      Para comunicacion mutua no se usara push y pull, estas se cambiaran por:
      push - REQ  (request) -> permite enviar y recibir
      pull - REP  (reply)   -> permite recibir y enviar
   */

  // open the connection
  cout << "Opening connection to " << endl;
  s.connect("tcp://localhost:4242");

  //leer matriz 1 y matriz 2
  ifstream Leermatriz1(argv[2]);
  ifstream Leermatriz2(argv[3]);

  int fila1, fila2; // fila 1 es para matriz 1
  int columna1, columna2; //columna 1 es para matriz 1
  string operacion;
  operacion = argv[1];

  Leermatriz1 >> fila1 >> columna1;
  Leermatriz2 >> fila2 >> columna2;

  int **Matriz1;
  int **Matriz2;

  //Creo el tamaño de las matrices de forma dinamica
  Matriz1 = (int **)malloc(fila1*sizeof(int*)); 

  for (int i = 0; i < fila1; i++){ 
   
    Matriz1[i] = (int*)malloc(columna1*sizeof(int));    
 
  }
  
  Matriz2 = (int **)malloc(fila2*sizeof(int*)); 
  
  for (int i = 0; i < fila2; i++){ 
 
    Matriz2[i] = (int*)malloc(columna2*sizeof(int));    
 
  }   


  if(operacion == "Multiplicacion"){

    if(fila1 == columna2 && fila2 == columna1){

      int **Matriz3;

      Matriz3 = (int **)malloc(fila1*sizeof(int*)); 

      for (int i = 0; i < fila1; i++){ 
          
        Matriz3[i] = (int*)malloc(columna2*sizeof(int));    
     
      }

      message req;

      //envio el tamaño de las filas y columnas
      req << operacion << fila1 << columna1 << fila2 << columna2;

      //Leo y envio la matriz 1
      for(int i = 0; i < fila1; i++){

        for(int j = 0; j < columna1; j++){

          Leermatriz1 >> Matriz1[i][j];

          req << Matriz1[i][j];

        }

      }

      //Leo y envio la matriz 2
      for(int i = 0; i < fila2; i++){

        for(int j = 0; j < columna2; j++){

          Leermatriz2 >> Matriz2[i][j];

          req << Matriz2[i][j]; //

        }

      }
      //send a message
      cout << "Sending text and a number..." << endl;
      s.send(req);

      // Recibir el mensaje
      cout << "Request sent." << endl;
      message rep;
      s.receive(rep);

      //matriz 3
      for(int i = 0;i < fila1; i++){
      
        for(int j = 0;j < columna2; j++){
      
            rep >> Matriz3[i][j];
        }

      }
      cout << "Matriz 1" << endl;
      mostrar_matriz(Matriz1, fila1, columna1);
      cout << "Matriz 2" << endl;
      mostrar_matriz(Matriz2, fila2, columna2);
      cout << "Matriz 3" << endl;
      mostrar_matriz(Matriz3, fila1, columna2);

    }else{

      cout << "No se pueden multiplicar las matrices" << endl;

    }

  }else if(operacion == "Inversa"){

    if(fila1 == fila2 && fila1 == columna2 && fila1 == columna1){

      double **Matriz3;
      double **Matriz4;

      Matriz3 = (double **)malloc(fila1*sizeof(double*)); 

      for (int i = 0; i < fila1; i++){ 
          
        Matriz3[i] = (double*)malloc(columna1*sizeof(double));    
     
      }

      Matriz4 = (double **)malloc(fila2*sizeof(double*)); 

      for (int i = 0; i < fila2; i++){ 
          
        Matriz4[i] = (double*)malloc(columna2*sizeof(double));    
     
      }


      message req;

      //envio el tamaño de las filas y columnas
      req << operacion << fila1 << columna1 << fila2 << columna2;

      //Leo y envio la matriz 1
      for(int i = 0; i < fila1; i++){

        for(int j = 0; j < columna1; j++){

          Leermatriz1 >> Matriz1[i][j];

          req << Matriz1[i][j];

        }

      }

      //Leo y envio la matriz 2
      for(int i = 0; i < fila2; i++){

        for(int j = 0; j < columna2; j++){

          Leermatriz2 >> Matriz2[i][j];

          req << Matriz2[i][j]; //

        }

      }
      //send a message
      cout << "Sending text and a number..." << endl;
      s.send(req);

      // Recibir el mensaje
      cout << "Request sent." << endl;
      message rep;
      
      s.receive(rep);

      int det1;
      int det2;
      
      rep >> det1 >> det2;

      if(det1 > 0){
        //matriz 3
        for(int i = 0;i < fila1; i++){
        
          for(int j = 0;j < columna1; j++){
        
              rep >> Matriz3[i][j];
          }

      }

        cout << "Matriz 1" << endl; 
        mostrar_matriz(Matriz1, fila1, columna1);
        cout << "Inversa Matriz 1" << endl;
        mostrar_inversa(Matriz3, fila1, columna1);
        cout << endl;
      }

      if(det2 > 0){

        //matriz 4
        for(int i = 0;i < fila2; i++){
        
          for(int j = 0;j < columna2; j++){
        
              rep >> Matriz4[i][j];
          }

        }
        cout << "Matriz 2" << endl; 
        mostrar_matriz(Matriz2, fila2, columna2);
        cout << "Inversa Matriz 2" << endl;
        mostrar_inversa(Matriz4, fila2, columna2);
        cout << endl;

      }else if(det1 == 0){

        cout << "Matriz 1 no tiene inversa" << endl;;
      
      }else if(det2 == 0){
      
        cout << "Matriz 2 no tiene inversa" << endl;
      
      }
        
    }else{

      cout << "No se puede realizar la inversa" << endl;
    }

    }else if(operacion == "Determinante"){

      if(fila1 == columna1 && fila2 == columna2){

      
        message req;

        //envio el tamaño de las filas y columnas
        req << operacion << fila1 << columna1 << fila2 << columna2;

        //Leo y envio la matriz 1
        for(int i = 0; i < fila1; i++){

          for(int j = 0; j < columna1; j++){

            Leermatriz1 >> Matriz1[i][j];

            req << Matriz1[i][j];

          }

        }

        //Leo y envio la matriz 2
        for(int i = 0; i < fila2; i++){

          for(int j = 0; j < columna2; j++){

            Leermatriz2 >> Matriz2[i][j];

            req << Matriz2[i][j]; //

          }

        }
        //send a message
        cout << "Sending text and a number..." << endl;
        s.send(req);

        // Recibir el mensaje
        cout << "Request sent." << endl;
        message rep;
        s.receive(rep);
        
        int det1;
        int det2;
        
        rep >> det1 >> det2;
        cout << "Determinante Matriz 1 = " << det1 << endl;
        cout << "Determinante Matriz 2 = " << det2 << endl;  
        

          
      }else{

        cout << "No se puede realizar la inversa" << endl;

      }
  }


  return 0;
}


void mostrar_matriz(int **Matriz, int fila, int columna){

  for (int i = 0; i < fila; i++){
    
    for (int j = 0; j < columna ; j++){
      
      cout << Matriz[i][j] << " ";     
    
    }
    
    cout << endl;
  }

}

void mostrar_inversa(double **Matriz, int fila, int columna){

  for (int i = 0; i < fila; i++){
    
    for (int j = 0; j < columna ; j++){
      
      cout << Matriz[i][j] << " ";     
    
    }
    
    cout << endl;
  }

}